﻿namespace IslamSandhesham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        Password = c.String(),
                        Description = c.String(),
                        IsLoggedIn = c.Boolean(nullable: false),
                        LastLoginTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Buttons",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SectionId = c.Int(),
                        Description = c.String(),
                        ImageId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Sections", t => t.SectionId)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        SectionId = c.Int(nullable: false),
                        Description = c.String(),
                        ClassName = c.String(),
                        Type = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Button_Id = c.Int(nullable: false),
                        Heading = c.String(),
                        Description = c.String(),
                        Info = c.String(),
                        ArabicText = c.String(),
                        SectionId = c.Int(),
                        ImageId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Buttons", t => t.Button_Id, cascadeDelete: true)
                .Index(t => t.Button_Id);
            
            CreateTable(
                "dbo.Homes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                        Description = c.String(),
                        Type = c.Int(),
                        Section = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Questions_Answers",
                c => new
                    {
                        Key = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        Question = c.String(),
                        Answer = c.String(),
                        Date = c.DateTime(),
                        Type = c.Int(),
                    })
                .PrimaryKey(t => t.Key);
            
            CreateTable(
                "dbo.TodayHadithVerses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(),
                        Reference = c.String(),
                        ArabicText = c.String(),
                        TeluguText = c.String(),
                        EnglishText = c.String(),
                        CreatedDate = c.DateTime(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "Button_Id", "dbo.Buttons");
            DropForeignKey("dbo.Buttons", "SectionId", "dbo.Sections");
            DropIndex("dbo.Contents", new[] { "Button_Id" });
            DropIndex("dbo.Buttons", new[] { "SectionId" });
            DropTable("dbo.TodayHadithVerses");
            DropTable("dbo.Questions_Answers");
            DropTable("dbo.Images");
            DropTable("dbo.Homes");
            DropTable("dbo.Contents");
            DropTable("dbo.Sections");
            DropTable("dbo.Buttons");
            DropTable("dbo.Admins");
        }
    }
}

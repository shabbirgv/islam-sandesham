﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IslamSandhesham.Models;

namespace IslamSandhesham.Controllers
{
    public class TodayHadithVersesController : Controller
    {
        private MyContext db = new MyContext();

        // GET: TodayHadithVerses
        public ActionResult Index()
        {
            return View(db.TodayHadithVerses.ToList());
        }
        public ActionResult ViewAll()
        {
            return View(db.TodayHadithVerses.ToList());
        }
        // GET: TodayHadithVerses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodayHadithVerse todayHadithVerse = db.TodayHadithVerses.Find(id);
            if (todayHadithVerse == null)
            {
                return HttpNotFound();
            }
            return View(todayHadithVerse);
        }

        // GET: TodayHadithVerses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TodayHadithVerses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Type,Reference,ArabicText,TeluguText,EnglishText,CreatedDate,UpdatedDate")] TodayHadithVerse todayHadithVerse)
        {
            if (ModelState.IsValid)
            {
                todayHadithVerse.CreatedDate = DateTime.Now;
                db.TodayHadithVerses.Add(todayHadithVerse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(todayHadithVerse);
        }

        // GET: TodayHadithVerses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodayHadithVerse todayHadithVerse = db.TodayHadithVerses.Find(id);
            if (todayHadithVerse == null)
            {
                return HttpNotFound();
            }
            return View(todayHadithVerse);
        }

        // POST: TodayHadithVerses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Type,Reference,ArabicText,TeluguText,EnglishText,CreatedDate,UpdatedDate")] TodayHadithVerse todayHadithVerse)
        {
            if (ModelState.IsValid)
            {
                todayHadithVerse.UpdatedDate = DateTime.Now;
                db.Entry(todayHadithVerse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(todayHadithVerse);
        }

        // GET: TodayHadithVerses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodayHadithVerse todayHadithVerse = db.TodayHadithVerses.Find(id);
            if (todayHadithVerse == null)
            {
                return HttpNotFound();
            }
            return View(todayHadithVerse);
        }

        // POST: TodayHadithVerses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TodayHadithVerse todayHadithVerse = db.TodayHadithVerses.Find(id);
            db.TodayHadithVerses.Remove(todayHadithVerse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

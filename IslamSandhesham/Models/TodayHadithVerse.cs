﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace IslamSandhesham.Models
{
    public class TodayHadithVerse
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? Type { get; set; }
        public string Reference { get; set; }
        public string ArabicText { get; set; }
        public string TeluguText { get; set; }
        public string EnglishText { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
    public enum Type
    {
        Hadith,
        Verse
    }
}